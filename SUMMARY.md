# Table of contents

## Instalación

* [Instalación en Windows](instalacion/instalacion-en-windows.md)
* [Instalación en Mac](instalacion/instalacion-en-mac.md)
* [Instalación en Linux](instalacion/instalacion-en-linux.md)

## Primeros Pasos

* [Configuración](primeros-pasos/configuracion.md)
